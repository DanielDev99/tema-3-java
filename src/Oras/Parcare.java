package Oras;

public class Parcare {
	
	private String numeParcare;
	private Integer numarLocuri;
	private Locatie loc;
	
	private Integer locuriOcupate;
	
	public Parcare(String numeParcare,Integer numarLocuri,Locatie loc)
	{
		this.numeParcare = numeParcare;
		this.numarLocuri = numarLocuri;
		this.loc = loc;
		this.locuriOcupate = 0;
	}
	
	public Integer getNrLocuriDisp() 
	{
		return this.numarLocuri - locuriOcupate;
	}
	
	public boolean suntLibere() 
	{
		return getNrLocuriDisp() > 0 ? true:false;
	}
	
	public String getNumeParcare()
	{
		return this.numeParcare;
	}
	
	@Override
	public String toString() 
	{
		return numeParcare + " " + getNrLocuriDisp() + " " + this.loc.toString();
	}
	
}
