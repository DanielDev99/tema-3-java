package Oras;


public class Locatie {
	
	private Integer N;
	private Integer S;
	private Integer V;
	private Integer E;
	
	public Locatie(Integer N,Integer S,Integer V,Integer E)
	{
		this.N = N;
		this.S = S;
		this.V = V;
		this.E = E;
	}
	
	public Integer getN() {
		return this.N;
	}
	
	public Integer getS()
	{
		return this.S;
	}
	
	public Integer getV()
	{
		return this.V;
	}
	
	public Integer getE()
	{
		return this.E;
	}
	
	public String toString()
	{
		return getN() + " " + getS() + " " + getV() +  " " + getE();
	}
	
}
