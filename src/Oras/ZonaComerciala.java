package Oras;
import java.util.ArrayList;

public class ZonaComerciala {
	
	private String numeZonaComerciala;
	private Locatie loc;
	
	private ArrayList<Magazin> magazine = new ArrayList<Magazin>();
	
	public ZonaComerciala(String numeZonaComerciala,Locatie loc)
	{
		this.numeZonaComerciala = numeZonaComerciala;
		this.loc = loc;
	}
	
	public ArrayList<Magazin> getListaMagazine()
	{
		return this.magazine;
	}
	
	public String toString()
	{
		return "Numele zonei este " + this.numeZonaComerciala;
	}
	
}
