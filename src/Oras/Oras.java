package Oras;
import java.util.ArrayList;


public class Oras {
	
	private String numeOras;
	
	private ArrayList<ZonaParcare> zoneCuParcari = new ArrayList<ZonaParcare>();
	
	private ArrayList<ZonaComerciala> zoneComerciale = new ArrayList<ZonaComerciala>();
	
	Locatie coordMasina;
	
	public Oras(String numeOras)
	{
		this.numeOras = numeOras;
		this.coordMasina = new Locatie(null,null,null,null);
	}
	
	public ArrayList<ZonaParcare> getListaParcari()
	{
		return this.zoneCuParcari;
	}
	
	public ArrayList<ZonaComerciala> getListaComerciale()
	{
		return this.zoneComerciale;
	}
	
	public Locatie getLocatie()
	{
		return this.coordMasina;
	}
	
	public void afisareParcMaxLocLibere()
	{
		Integer locLibMax = 0;
		Parcare parcareLocLibMax = null;
		
		for(ZonaParcare zona : zoneCuParcari)
		{
			for(Parcare parcare : zona.getParcari())
			{
				if(parcare.getNrLocuriDisp()>locLibMax)
				{
					locLibMax = parcare.getNrLocuriDisp();
					parcareLocLibMax = parcare;
				}
			}
		}
		
		System.out.println(parcareLocLibMax);
	}
	
	public String toString()
	{
		return "Numele orasului este " + this.numeOras + " iar masina se afara la coordonatele" + coordMasina;
	}
	
}
