package Oras;

public class Magazin {
	
	String numeMagazin;
	
	public enum Categorie
	{
		none,
		fastfood,
		farmacie,
		patiserie,
		supermarket,
		minimarket
	}
	
	Locatie loc;
	
	Categorie tipMagazin;
	
	public Magazin(String numeMagazin,Locatie loc,String tipMagazin)
	{
		this.numeMagazin = numeMagazin;
		this.loc = loc;
		this.tipMagazin = convertireTip(tipMagazin);
	}
	
	private Categorie convertireTip(String tip)
	{
		for(Categorie constanta : Categorie.values())
		{
			if(constanta.toString() == tip) return constanta;
		}
		return Categorie.none;
	}
	
}
