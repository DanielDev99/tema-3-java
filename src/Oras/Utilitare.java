package Oras;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Utilitare 
{
	
	public static Optional<JSONArray> citireConfiguratie(String fileName)
	{
		
		JSONParser parser = new JSONParser();
		
		Optional<JSONArray> jsonArray = Optional.empty();
		
		try 
		{
		
			Object obj = parser.parse(new FileReader(fileName));
			
			
			jsonArray = Optional.of((JSONArray) obj);
	
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		
		return jsonArray;
		
	}
	
	public static void scriereZoneParcari(Oras oras,JSONArray configuratie)
	{
		for(Integer i = 0;i<configuratie.size();++i)
		{
			JSONObject obiect = (JSONObject) configuratie.get(i);
			
			ZonaParcare zona = new ZonaParcare((String) obiect.get("nume"));
			
			JSONArray parcari = (JSONArray) obiect.get("parcari");
			
			for(Integer j = 0;j<parcari.size();++j)
			{
				obiect = (JSONObject) parcari.get(j);
				
				Locatie locatieParcare = creareLocatie(obiect);
				
				zona.getParcari()
				.add(new Parcare(
						(String) obiect.get("nume"),
						(int) (long) obiect.get("locuri")
						,locatieParcare));	
				
				}
			
			oras.getListaParcari().add(zona);
		}
	}
	
	public static void scriereZoneComerciale(Oras oras,JSONArray configuratie)
	{
		
		for(Integer i = 0;i<configuratie.size();++i)
		{
			
			JSONObject obiect = (JSONObject) configuratie.get(i);
			
			Locatie locatieZona = creareLocatie(obiect);
			
			ZonaComerciala zona = new ZonaComerciala((String) obiect.get("nume"),locatieZona);

			JSONArray mag = (JSONArray) obiect.get("magazine");
		
			for(Integer j = 0;j<mag.size();++j)
			{
				obiect = (JSONObject) mag.get(j);
				
				Locatie locatieMagazin = creareLocatie(obiect);
				
				zona.getListaMagazine()
				.add(new Magazin(
						(String) obiect.get("nume"),
						locatieMagazin,
						(String) obiect.get("tip")
						));
					
			}
		
			oras.getListaComerciale().add(zona);
		}

	}
	
	public static Locatie creareLocatie(JSONObject obiect)
	{
		return new Locatie(
						(int) (long) obiect.get("N"),
						(int) (long) obiect.get("S"),
						(int) (long) obiect.get("V"),
						(int) (long) obiect.get("E"));
	}
	
}
