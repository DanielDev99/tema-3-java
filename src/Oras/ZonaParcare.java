package Oras;

import java.util.ArrayList;

public class ZonaParcare {
	
	private String numeZonaParcare;
	
	private ArrayList<Parcare> parcari = new ArrayList<Parcare>();
	
	public ZonaParcare(String numeZonaParcare)
	{
		this.numeZonaParcare = numeZonaParcare;
	}
	
	public ArrayList<Parcare> getParcari()
	{
		return this.parcari;
	}
	
	@Override
	public String toString()
	{
		return this.numeZonaParcare;
	}
	
}
