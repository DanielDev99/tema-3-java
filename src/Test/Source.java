package Test;

import java.util.Optional;

import org.json.simple.JSONArray;
import Oras.Oras;
import Oras.Utilitare;



public class Source {

	public static void main(String[] args)
	{
		
		Oras Brasov = new Oras("Brasov");
		
		Optional<JSONArray> configuratieComerciala = Utilitare.citireConfiguratie("comercial.json");
		Optional<JSONArray> configuratieParcari = Utilitare.citireConfiguratie("parcari.json");
		
		if(configuratieComerciala.isPresent()){
			Utilitare.scriereZoneComerciale(Brasov, configuratieComerciala.get());
		}
		if(configuratieParcari.isPresent()) {
			Utilitare.scriereZoneParcari(Brasov,configuratieParcari.get());
		}
	
		Brasov.afisareParcMaxLocLibere();
		
	}
	
	
	
	
	
	
	
}
